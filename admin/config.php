<?php
// HTTP
define('HTTP_SERVER', 'http://dev5.dreamvention.com/admin/');
define('HTTP_CATALOG', 'http://dev5.dreamvention.com/');

// HTTPS
define('HTTPS_SERVER', 'http://dev5.dreamvention.com/admin/');
define('HTTPS_CATALOG', 'http://dev5.dreamvention.com/');

// DIR
define('DIR_APPLICATION', '/home/dev5/public_html/admin/');
define('DIR_SYSTEM', '/home/dev5/public_html/system/');
define('DIR_LANGUAGE', '/home/dev5/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/dev5/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/dev5/public_html/system/config/');
define('DIR_IMAGE', '/home/dev5/public_html/image/');
define('DIR_CACHE', '/home/dev5/public_html/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/dev5/public_html/system/storage/download/');
define('DIR_LOGS', '/home/dev5/public_html/system/storage/logs/');
define('DIR_MODIFICATION', '/home/dev5/public_html/system/storage/modification/');
define('DIR_UPLOAD', '/home/dev5/public_html/system/storage/upload/');
define('DIR_CATALOG', '/home/dev5/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'user');
define('DB_PASSWORD', 'user1234');
define('DB_DATABASE', 'developer_5');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
